﻿using Escuela0.Models;
using System;

namespace Escuela0
{
    class Program
    {
        static void Main(string[] args)
        {


            Console.WriteLine("Selecciones las opciones de la base de datos deseada");
            Console.WriteLine("1) Crear Alumno");
            Console.WriteLine("2) Leer ALumno");
            Console.WriteLine("3) Actualizar ALumno");
            Console.WriteLine("4) Eliminar alumno");
            Console.WriteLine("5) Crear Maestro");
            Console.WriteLine("6) Leer Maestro");
            Console.WriteLine("7) Actualizar Maestro");
            Console.WriteLine("8) Eliminar Maestro");


            int caseSwitch = int.Parse(Console.ReadLine());

            switch (caseSwitch)
            {
                case 1:
                    Console.WriteLine("Ha seleccionado CREAR");
                    CrearAlumno();

                    break;
                case 2:
                    Console.WriteLine("Ha seleccionado LEER");
                    ListarAlumnos();

                    break;

                case 3:
                    Console.WriteLine("Ha seleccionado Actualizar");


                    break;
                case 4:
                    Console.WriteLine("Ha seleccionado Eliminar");

                    break;
                case 5:
                    Console.WriteLine("Ha seleccionado Crear");

                    break;
                case 6:
                    Console.WriteLine("Ha seleccionado Leer");

                    break;
                case 7:
                    Console.WriteLine("Ha seleccionado Actualizar");

                    break;
                case 8:
                    Console.WriteLine("Ha seleccionado Eliminar");

                    break;
                
                default:
                    Console.WriteLine("No ha elegido ninguna opción");
                    Console.WriteLine("CERRANDO PROGRAMA");
                    break;
            }



        }

        private static void CrearAlumno()
        {
            Console.WriteLine("Nuevo Alumno");
            Alumnos alumno = new Alumnos();
            Console.Write("Id: ");
            alumno.Id = Console.Read();
            Console.Write("Nombre: ");
            alumno.Nombre = Console.ReadLine();
            Console.Write("Apellidos: ");
            alumno.Apellidos = Console.ReadLine();
            Console.Write("Edad: ");
            alumno.Edad = Console.ReadLine();

            using (EscuelaContext context = new EscuelaContext())
            {
                context.Add(alumno);
                context.SaveChanges();
                Console.WriteLine("Alumno guardado");
            }


        }

        public static void ListarAlumnos()
        {
            Console.WriteLine("Lista de Alumnos");
            using (EscuelaContext context = new EscuelaContext())
            {
                foreach (Alumnos alumnos in context.Alumnos)
                {
                    Console.WriteLine($"{alumnos.Id}) {alumnos.Nombre} {alumnos.Apellidos} {alumnos.Edad} ");

                }
            }
        }

        private static void CrearMaestro()
        {
            Console.WriteLine("Nuevo Profesor");
            Profesor profesor = new Profesor();
            Console.Write("Id: ");
            profesor.Id = Console.Read();
            Console.Write("Nombre: ");
            profesor.Nombre = Console.ReadLine();
            Console.Write("Apellidos: ");
            profesor.Apellidos = Console.ReadLine();
            

            using (EscuelaContext context = new EscuelaContext())
            {
                context.Add(profesor);
                context.SaveChanges();
                Console.WriteLine("Profesor guardado");
            }


        }

        public static void ListarMaestro()
        {
            Console.WriteLine("Lista de Clientes");
            using (EscuelaContext context = new EscuelaContext())
            {
                foreach (Alumnos alumnos in context.Alumnos)
                {
                    Console.WriteLine($"{alumnos.Id}) {alumnos.Nombre} {alumnos.Apellidos} {alumnos.Edad} ");

                }
            }
        }
    }
}

