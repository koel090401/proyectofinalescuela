﻿using System;
using System.Collections.Generic;

namespace Escuela0.Models
{
    public partial class Profesor
    {
        public Profesor()
        {
            Cursos = new HashSet<Cursos>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }

        public virtual ICollection<Cursos> Cursos { get; set; }
    }
}
