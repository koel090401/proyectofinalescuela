﻿using System;
using System.Collections.Generic;

namespace Escuela0.Models
{
    public partial class Cursos
    {
        public int Id { get; set; }
        public string Tipo { get; set; }
        public int? IdAlumno { get; set; }
        public int? IdProf { get; set; }
        public int? IdMat { get; set; }

        public virtual Alumnos IdAlumnoNavigation { get; set; }
        public virtual Materia IdMatNavigation { get; set; }
        public virtual Profesor IdProfNavigation { get; set; }
    }
}
