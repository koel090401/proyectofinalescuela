﻿using System;
using System.Collections.Generic;

namespace Escuela0.Models
{
    public partial class Feria
    {
        public int Id { get; set; }
        public int IdAlumn { get; set; }
        public string Tipo { get; set; }

        public virtual Alumnos IdAlumnNavigation { get; set; }
    }
}
