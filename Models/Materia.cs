﻿using System;
using System.Collections.Generic;

namespace Escuela0.Models
{
    public partial class Materia
    {
        public Materia()
        {
            Cursos = new HashSet<Cursos>();
        }

        public int Id { get; set; }
        public string Tipo { get; set; }

        public virtual ICollection<Cursos> Cursos { get; set; }
    }
}
