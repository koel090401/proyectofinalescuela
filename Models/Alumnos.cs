﻿using System;
using System.Collections.Generic;

namespace Escuela0.Models
{
    public partial class Alumnos
    {
        public Alumnos()
        {
            Cursos = new HashSet<Cursos>();
            Feria = new HashSet<Feria>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Edad { get; set; }

        public virtual ICollection<Cursos> Cursos { get; set; }
        public virtual ICollection<Feria> Feria { get; set; }
    }
}
