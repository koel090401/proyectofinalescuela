﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Escuela0.Models
{
    public partial class EscuelaContext : DbContext
    {
        public EscuelaContext()
        {
        }

        public EscuelaContext(DbContextOptions<EscuelaContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Alumnos> Alumnos { get; set; }
        public virtual DbSet<Cursos> Cursos { get; set; }
        public virtual DbSet<Feria> Feria { get; set; }
        public virtual DbSet<Materia> Materia { get; set; }
        public virtual DbSet<Profesor> Profesor { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySql("server=localhost;port=3306;user=root;database=escuela", x => x.ServerVersion("10.4.11-mariadb"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Alumnos>(entity =>
            {
                entity.ToTable("alumnos");

                entity.Property(e => e.Id).HasColumnType("int(20)");

                entity.Property(e => e.Apellidos)
                    .IsRequired()
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Edad).HasColumnType("int(2)");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            modelBuilder.Entity<Cursos>(entity =>
            {
                entity.ToTable("cursos");

                entity.HasIndex(e => e.IdAlumno)
                    .HasName("Id_Alumno");

                entity.HasIndex(e => e.IdMat)
                    .HasName("Id_Mat");

                entity.HasIndex(e => e.IdProf)
                    .HasName("Id_Prof");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.IdAlumno)
                    .HasColumnName("Id_Alumno")
                    .HasColumnType("int(20)");

                entity.Property(e => e.IdMat)
                    .HasColumnName("Id_Mat")
                    .HasColumnType("int(20)");

                entity.Property(e => e.IdProf)
                    .HasColumnName("Id_Prof")
                    .HasColumnType("int(20)");

                entity.HasOne(d => d.IdAlumnoNavigation)
                    .WithMany(p => p.Cursos)
                    .HasForeignKey(d => d.IdAlumno)
                    .HasConstraintName("cursos_ibfk_1");

                entity.HasOne(d => d.IdMatNavigation)
                    .WithMany(p => p.Cursos)
                    .HasForeignKey(d => d.IdMat)
                    .HasConstraintName("cursos_ibfk_3");

                entity.HasOne(d => d.IdProfNavigation)
                    .WithMany(p => p.Cursos)
                    .HasForeignKey(d => d.IdProf)
                    .HasConstraintName("cursos_ibfk_2");
            });

            modelBuilder.Entity<Feria>(entity =>
            {
                entity.ToTable("feria");

                entity.HasIndex(e => e.IdAlumn)
                    .HasName("Id_Alumn");

                entity.Property(e => e.Id).HasColumnType("int(20)");

                entity.Property(e => e.IdAlumn)
                    .HasColumnName("Id_Alumn")
                    .HasColumnType("int(20)");

                entity.Property(e => e.Tipo)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.HasOne(d => d.IdAlumnNavigation)
                    .WithMany(p => p.Feria)
                    .HasForeignKey(d => d.IdAlumn)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("feria_ibfk_1");
            });

            modelBuilder.Entity<Materia>(entity =>
            {
                entity.ToTable("materia");

                entity.Property(e => e.Id).HasColumnType("int(20)");

                entity.Property(e => e.Tipo)
                    .IsRequired()
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            modelBuilder.Entity<Profesor>(entity =>
            {
                entity.ToTable("profesor");

                entity.Property(e => e.Id).HasColumnType("int(20)");

                entity.Property(e => e.Apellidos)
                    .IsRequired()
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
